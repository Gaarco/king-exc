#include <stdio.h>

int main(void) {
    float radius;

    printf("Please insert the radius of the sphere: ");
    scanf("%f", &radius);

    float volume = 4.0f / 3.0f * 3.14f * radius * radius;

    printf("Sphere volume: %.2f\n", volume);

    return 0;
}
