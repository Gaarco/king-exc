#include <stdio.h>

int main(void) {
    int amount;

    printf("Please enter an amount: ");
    scanf("%d", &amount);

    int count = amount / 20;
    printf("$20 bills: %d\n", count);

    amount = amount - 20 * count;
    count = amount / 10;
    printf("$10 bills: %d\n", count);

    amount = amount - 10 * count;
    count = amount / 5;
    printf("$5 bills: %d\n", count);

    amount = amount - 5 * count;
    count = amount / 1;
    printf("$1 bills: %d\n", count);

    return 0;
}
