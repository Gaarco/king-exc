#include <stdio.h>

int main(void) {
    int value;

    printf("Please enter a value: ");
    scanf("%d", &value);

    int result = 3 * value * value * value * value * value + 2 * value * value * value * value
        - 5 * value * value * value - value * value + 7 * value - 6;
    printf("Value of the polynomial: %d\n", result);

    return 0;
}
