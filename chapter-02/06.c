#include <stdio.h>

int main(void) {
    int x;

    printf("Please enter a value: ");
    scanf("%d", &x);

    int result = ((((3 * x + 2) * x - 5) * x - 1) * x + 7) * x - 6;
    printf("Value of the polynomial: %d\n", result);

    return 0;
}
