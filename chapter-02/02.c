#include <stdio.h>

int main(void) {
    float radius = 10;
    float volume = 4.0f / 3.0f * 3.14f * radius * radius;

    printf("Sphere volume: %.2f\n", volume);

    return 0;
}
