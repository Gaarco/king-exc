#ifndef FLIGHT_H
#define FLIGHT_H

struct flight {
    int dep;
    int arr;
};

void print_flight(const struct flight time);

#endif
