#include "flight.h"
#include <stdio.h>

void print_flight(const struct flight time) {
    printf("%d:%.2d %c.m. arriving at %d:%.2d %c.m.\n",
        time.dep / 60 > 12 ? time.dep / 60 - 12 : time.dep / 60, time.dep % 60,
        time.dep >= 60 * 12 ? 'p' : 'a', time.arr / 60 > 12 ? time.arr / 60 - 12 : time.arr / 60,
        time.arr % 60, time.arr >= 60 * 12 ? 'p' : 'a');
}
