#include "flight.h"
#include <stdio.h>

#define TIMES_COUNT ((int) (sizeof(times) / sizeof(times[0])))

const struct flight times[] = { { 480, 616 }, { 583, 712 }, { 679, 811 }, { 767, 900 },
    { 840, 968 }, { 945, 1075 }, { 1140, 1280 }, { 1305, 1438 } };

int main(void) {
    int hours, minutes, minutes_from_midnight;

    printf("Enter a 24-hour time: ");
    scanf("%2d:%2d", &hours, &minutes);

    minutes_from_midnight = hours * 60 + minutes;

    for (int i = 0; i < TIMES_COUNT - 1; i++) {
        if (minutes_from_midnight < times[i].dep + (times[i + 1].dep - times[i].dep) / 2) {
            print_flight(times[i]);
            return 0;
        } else if (i == TIMES_COUNT - 1) {
            print_flight(times[i + 1]);
        }
    }

    return 0;
}
