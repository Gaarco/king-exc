#include "date.h"

int day_of_year(const struct date d) {
    int days = 0;

    const int month_days[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 30, 31, 31 };

    if (d.m > 2 && ((d.y % 4 == 0 && d.y % 100 != 0) || d.y % 400 == 0)) {
        days++;
    }

    for (int i = 0; i < d.m - 1; i++) {
        days += month_days[i];
    }

    return days + d.d;
}

/*
 * Compares two dates
 * Returns 0 if d1 == d2
 * Returns 1 if d1 > d2
 * Returns -1 if d1 < d2
 */
int compare_dates(struct date d1, struct date d2) {
    int d1_days = day_of_year(d1);
    int d2_days = day_of_year(d2);

    if (d1.y > d2.y || d1_days > d2_days) {
        return 1;
    } else if (d1.y < d2.y || d1_days < d2_days) {
        return -1;
    } else {
        return 0;
    }
}
