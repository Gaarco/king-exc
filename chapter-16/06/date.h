#ifndef DATE_H
#define DATE_H

struct date {
    int d;
    int m;
    int y;
};

int compare_dates(struct date d1, struct date d2);

#endif
