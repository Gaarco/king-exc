#include "date.h"
#include <stdio.h>

int main(void) {
    struct date d1, d2;

    printf("Enter first date (mm/dd/yy): ");
    scanf("%d/%d/%d", &d1.m, &d1.d, &d1.y);
    printf("Enter second date (mm/dd/yy): ");
    scanf("%d/%d/%d", &d2.m, &d2.d, &d2.y);

    int result = compare_dates(d1, d2);

    if (result < 0) {
        printf("%2d/%2d/%2d is earlier than %2d/%2d/%2d\n", d1.m, d1.d, d1.y, d2.m, d2.d, d2.y);
    } else if (result > 0) {
        printf("%2d/%2d/%2d is earlier than %2d/%2d/%2d\n", d2.m, d2.d, d2.y, d1.m, d1.d, d1.y);
    } else {
        printf("The dates are equal\n");
    }

    return 0;
}
