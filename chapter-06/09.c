#include <stdio.h>

int main(void) {
    int number_payments;
    float loan, interest, monthly_payment;

    printf("Enter amount of loan: ");
    scanf("%f", &loan);

    printf("Enter interest rate: ");
    scanf("%f", &interest);

    printf("Enter monthly payment: ");
    scanf("%f", &monthly_payment);

    printf("Enter number of payments: ");
    scanf("%d", &number_payments);

    for (int i = 0; i < number_payments; i++) {
        loan = loan - monthly_payment + loan * interest / 100 / 12;
        printf("Balance remaining after %d payment: %.2f\n", i, loan);
    }

    return 0;
}
