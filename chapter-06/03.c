#include <stdio.h>

int main(void) {
    int num, den;

    printf("Enter a fraction: ");
    scanf("%d/%d", &num, &den);

    int max, min;
    if (num > den) {
        max = num;
        min = den;
    } else {
        max = den;
        min = num;
    }

    while (min != 0) {
        int r = max % min;
        max = min;
        min = r;
    }

    printf("In lowest terms: %d/%d\n", num / max, den / max);

    return 0;
}
