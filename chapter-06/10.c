#include <stdio.h>

int main(void) {
    int d, m, y, ed, em, ey;

    printf("Enter a date (mm/dd/yy): ");
    scanf("%d/%d/%d", &m, &d, &y);
    ed = d;
    em = m;
    ey = y;

    while (d != 0 && m != 0 && y != 0) {
        if ((y < ey) || (y == ey && m < em) || (y == ey && m == em && d < ed)) {
            ed = d;
            em = m;
            ey = y;
        }

        printf("Enter a date (mm/dd/yy): ");
        scanf("%d/%d/%d", &m, &d, &y);
    }

    printf("%.2d/%.2d/%.2d is the earliest date\n", em, ed, ey);

    return 0;
}
