#include <stdio.h>

int main(void) {
    int m, n, max, min;

    printf("Enter two integers: ");
    scanf("%d%d", &m, &n);

    if (m > n) {
        max = m;
        min = n;
    } else {
        max = n;
        min = m;
    }

    while (min != 0) {
        int r = max % min;
        max = min;
        min = r;
    }

    printf("Greatest common divisor: %d\n", max);

    return 0;
}
