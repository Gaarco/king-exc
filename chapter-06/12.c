#include <stdio.h>

int main(void) {
    int fac = 1;
    float e = 1.0f, epsilon;

    printf("Enter epsilon: ");
    scanf("%f", &epsilon);

    for (int i = 1; epsilon <= 1.0f / (fac *= i); i++) {
        e += 1.0f / fac;
    }

    printf("Value of e: %.6f\n", e);

    return 0;
}
