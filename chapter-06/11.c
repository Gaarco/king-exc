#include <stdio.h>

int main(void) {
    int n, fac = 1;
    float e = 1.0f;

    printf("Enter a number: ");
    scanf("%d", &n);

    for (int i = 1; i <= n; i++) {
        fac *= i;
        e += 1.0f / fac;
    }

    printf("Value of e: %.6f\n", e);

    return 0;
}
