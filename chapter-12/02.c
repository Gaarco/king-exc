#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_LEN 50

int main(void) {
    char c, input[MAX_LEN], *i, *j;

    printf("Enter a message: ");

    i = &input[0];
    while ((c = getchar()) != '\n' && i < &input[0] + MAX_LEN) {
        if (isalpha(c)) {
            *i++ = tolower(c);
        }
    }

    j = input;
    while (i > &input[0]) {
        if (*--i != *j++) {
            printf("Not a palindrome.\n");
            exit(EXIT_SUCCESS);
        }
    }

    printf("Palindrome.\n");

    return 0;
}
