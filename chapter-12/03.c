#include <stdio.h>

#define MAX_LEN 50

int main(void) {
    char c;
    char input[MAX_LEN];

    printf("Enter a message: ");

    char *i = input;
    while ((c = getchar()) != '\n' && i < input + MAX_LEN) {
        *i++ = c;
    }

    printf("Reversal is: ");
    while (i > input) {
        printf("%c", *--i);
    }
    printf("\n");

    return 0;
}
