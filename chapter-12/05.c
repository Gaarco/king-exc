#include <stdio.h>

#define LEN 50

int main(void) {
    char term, sentence[LEN] = { 0 };

    printf("Enter a sentence: ");

    for (char *i = sentence; i < sentence + LEN; i++) {
        char c = getchar();
        if (c == '.' || c == '?' || c == '!') {
            term = c;
            break;
        }
        *i = c;
    }

    char *end_word = sentence + LEN;
    for (char *i = sentence + LEN; i >= sentence; i--) {
        if (i == sentence || *(i - 1) == ' ') {
            for (char *j = i; j < end_word; j++) {
                putchar(*j);
            }
            putchar(i == sentence ? term : ' ');
            end_word = i - 1;
        }
    }
    putchar('\n');

    return 0;
}
