#include <ctype.h>
#include <stdio.h>

int compute_vowel_count(const char *sentence);

int main(void) {
    char sentence[100];

    printf("Enter a sentence: ");
    scanf("%s", sentence);

    int vc = compute_vowel_count(sentence);

    printf("Your sentence contains %d vowels\n", vc);

    return 0;
}

int compute_vowel_count(const char *sentence) {
    int res = 0;

    for (int i = 0; sentence[i] != '\0'; i++) {
        if (sentence[i] == 'A' || sentence[i] == 'E' || sentence[i] == 'I' || sentence[i] == 'O'
            || sentence[i] == 'U') {
            res++;
        }
    }

    return res;
}
