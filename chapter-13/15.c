#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#define STACK_SIZE 100
#define LEN 100

int contents[STACK_SIZE];
int top = 0;

int evaluate_RPN_expression(const char *expr);
void stack_overflow(void);
void stack_underflow(void);
void make_empty(void);
bool is_empty(void);
bool is_full(void);
void push(int i);
int pop(void);

int main(void) {
    char expr[LEN] = { 0 };

    while (true) {
        printf("Enter an RPN expression: ");
        fgets(expr, LEN, stdin);

        printf("Value of expression: %d\n", evaluate_RPN_expression(expr));
    }

    return 0;
}

void stack_overflow(void) {
    printf("Expression is too complex\n");
    exit(EXIT_FAILURE);
}

void stack_underflow(void) {
    printf("Not enough operands in expression\n");
    exit(EXIT_FAILURE);
}

void make_empty(void) { top = 0; }

bool is_empty(void) { return top == 0; }

bool is_full(void) { return top == STACK_SIZE; }

void push(int i) {
    if (is_full()) {
        stack_overflow();
    }
    contents[top++] = i;
}

int pop(void) {
    if (is_empty()) {
        stack_underflow();
    }
    return contents[--top];
}

int evaluate_RPN_expression(const char *expr) {
    int result;
    char op1, op2;

    while (*expr != '\n') {
        if (isdigit(*expr)) {
            push(*expr - '0');
        } else {
            switch (*expr) {
                case '+':
                    op1 = pop();
                    op2 = pop();
                    push(op2 + op1);
                    break;
                case '-':
                    op1 = pop();
                    op2 = pop();
                    push(op2 - op1);
                    break;
                case '*':
                    op1 = pop();
                    op2 = pop();
                    push(op2 * op1);
                    break;
                case '/':
                    op1 = pop();
                    op2 = pop();
                    push(op2 / op1);
                    break;
                case '=':
                    result = pop();
                    make_empty();
                    break;
                case ' ':
                    break;
                default:
                    exit(EXIT_SUCCESS);
                    break;
            }
        }
        expr++;
    }
    return result;
}
