#include <ctype.h>
#include <stdio.h>

#define MAX_LEN 50

bool is_palindrome(const char *message);

int main(void) {
    char c, input[MAX_LEN] = { 0 }, *p = input;

    printf("Enter a message: ");
    while ((c = getchar()) != '\n' && p < input + MAX_LEN) {
        if (isalpha(c)) {
            *p++ = c;
        }
    }

    printf("%s.\n", is_palindrome(input) ? "Palindrome" : "Not a palindrome");

    return 0;
}

bool is_palindrome(const char *message) {
    const char *first = message, *last = message;

    while (*last) {
        last++;
    }
    last--;

    while (first < last) {
        if (tolower(*first) != tolower(*last)) {
            return false;
        }
        first++;
        last--;
    }

    return true;
}
