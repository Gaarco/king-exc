#include <ctype.h>
#include <stdio.h>

#define LEN 100

bool are_anagrams(const char *word1, const char *word2);

int main(void) {
    char word1[LEN], word2[LEN], c;

    int i = 0;
    printf("Enter first word: ");
    while ((c = getchar()) != '\n' && i < LEN) {
        if (isalpha(c)) {
            word1[i++] = toupper(c);
        }
    }
    word1[i] = '\0';

    i = 0;
    printf("Enter second word: ");
    while ((c = getchar()) != '\n' && i < LEN) {
        if (isalpha(c)) {
            word2[i++] = toupper(c);
        }
    }
    word2[i] = '\0';

    printf("The words %s anagrams.\n", are_anagrams(word1, word2) ? "are" : "are not");

    return 0;
}

bool are_anagrams(const char *word1, const char *word2) {
    int alphabet[26] = { 0 };

    while (*word1) {
        alphabet[*word1 - 'A']++;
        word1++;
    }

    while (*word2) {
        alphabet[*word2 - 'A']--;
        word2++;
    }

    for (int i = 0; i < 26; i++) {
        if (alphabet[i] != 0) {
            return false;
        }
    }

    return true;
}
