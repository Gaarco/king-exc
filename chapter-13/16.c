#include <stdio.h>

#define MAX_LEN 50

void reverse(char *message);

int main(void) {
    char input[MAX_LEN] = { 0 };

    printf("Enter a message: ");
    fgets(input, MAX_LEN, stdin);

    reverse(input);
    printf("Reversal is: %s", input);

    return 0;
}

void reverse(char *message) {
    char *first = message, *last = message;

    while (*last != '\n') {
        last++;
    }
    last--; // Leave '\n' at the end

    while (first < last) {
        char tmp = *first;

        *first = *last;
        *last = tmp;

        first++;
        last--;
    }
}
