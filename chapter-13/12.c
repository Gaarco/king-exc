#include <ctype.h>
#include <stdio.h>

#define MAX_WORD 30
#define MAX_WORD_LEN 20

int main(void) {
    char term, sentences[MAX_WORD][MAX_WORD_LEN + 1] = { '\0' };

    printf("Enter a sentence: ");

    char c;
    int w_count = 0, c_count = 0;
    while ((c = getchar()) != '\n' && w_count < MAX_WORD) {
        if (c == ' ') {
            w_count++;
            c_count = 0;
            continue;
        }
        if (c == '.' || c == '?' || c == '!') {
            term = c;
            break;
        }
        sentences[w_count][c_count++] = c;
    }

    while (w_count > 0) {
        printf("%s ", sentences[w_count--]);
    }
    printf("%s%c\n", sentences[w_count], term);

    return 0;
}
