#include <stdio.h>

int main(void) {
    int num, first, second;

    const char *x[]
        = { "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
    const char *y[] = { "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen",
        "seventeen", "eighteen", "nineteen" };
    const char *z[]
        = { "", "-one", "-two", "-three", "-four", "-five", "-six", "-seven", "-eight", "-nine" };

    printf("Enter a two-digit number: ");
    scanf("%2d", &num);

    first = num / 10;
    second = num % 10;

    if (first == 1) {
        printf("%s.\n", y[second]);
    } else {
        printf("%s", x[first - 2]);
        printf("%s.\n", z[second]);
    }

    return 0;
}
