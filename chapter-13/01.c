#include <stdio.h>
#include <string.h>

#define MAX_SIZE 20

int main(void) {
    char word[MAX_SIZE + 1], smallest[MAX_SIZE + 1], largest[MAX_SIZE + 1];

    do {
        printf("Enter word: ");

        scanf("%s", word);

        if (!*smallest || !*largest) {
            strcpy(smallest, word);
            strcpy(largest, word);
        }

        if (strcmp(word, smallest) < 0) {
            strcpy(smallest, word);
        }

        if (strcmp(word, largest) > 0) {
            strcpy(largest, word);
        }
    } while (strlen(word) != 4);

    printf("Smallest word: %s\n", smallest);
    printf("Largest word: %s\n", largest);

    return 0;
}
