#include <stdio.h>

void reverse_name(char *name);

int main(void) {
    char name[100];

    printf("Enter a first and last name: ");
    fgets(name, sizeof(name), stdin);

    reverse_name(name);

    printf("%s\n", name);

    return 0;
}

void reverse_name(char *name) {
    char *p = name, first;

    // Skip white characters
    while (*p && *p == ' ') {
        p++;
    }
    first = *p;

    // Go to the first space after first name
    while (*p && *p != ' ') {
        p++;
    }

    // Skip white characters
    while (*p && *p == ' ') {
        p++;
    }

    int len; // len is the length of the last name
    for (len = 0; p[len] != '\n' && p[len] && p[len] != ' '; len++) { }
    sprintf(name, "%.*s, %c.", len, p, first);
}
