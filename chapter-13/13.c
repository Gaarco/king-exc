#include <stdio.h>

#define LEN 80

void encrypt(char *message, int shift);

int main(void) {
    char mess[LEN] = { 0 };
    int shift;

    printf("Enter message to be encrypted: ");
    fgets(mess, LEN, stdin);

    printf("Enter shift amount (1-25): ");
    scanf("%d", &shift);

    encrypt(mess, shift);

    printf("Encrypted message: %s", mess);

    return 0;
}

void encrypt(char *message, int shift) {
    for (int i = 0; i < LEN; i++) {
        if (message[i] >= 'A' && message[i] <= 'Z') {
            message[i] = ((message[i] - 'A') + shift) % 26 + 'A';
        } else if (message[i] >= 'a' && message[i] <= 'z') {
            message[i] = ((message[i] - 'a') + shift) % 26 + 'a';
        }
    }
}
