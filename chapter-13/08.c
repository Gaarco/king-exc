#include <ctype.h>
#include <stdio.h>

int compute_scrabble_value(const char *word);

int main(void) {
    char word[100];

    printf("Enter a word: ");

    scanf("%s", word);

    int s_val = compute_scrabble_value(word);

    printf("Scrabble value: %d\n", s_val);

    return 0;
}

int compute_scrabble_value(const char *word) {
    int res = 0;
    for (int i = 0; word[i] != '\0'; i++) {
        switch (toupper(word[i])) {
            case 'D':
            case 'G':
                res += 2;
                break;
            case 'B':
            case 'C':
            case 'M':
            case 'P':
                res += 3;
                break;
            case 'F':
            case 'H':
            case 'V':
            case 'W':
            case 'Y':
                res += 4;
                break;
            case 'K':
                res += 5;
                break;
            case 'J':
            case 'X':
                res += 8;
                break;
            case 'Q':
            case 'Z':
                res += 10;
                break;
            default:
                res += 1;
        }
    }
    return res;
}
