#include <stdio.h>
#include <string.h>

double compute_average_word_length(const char *sentence);

int main(void) {
    char sentence[100];

    printf("Enter a sentence: ");
    fgets(sentence, sizeof(sentence), stdin);
    printf("Average word length: %.1f\n", compute_average_word_length(sentence));

    return 0;
}

double compute_average_word_length(const char *sentence) {
    int w_count = 1, l_count = 0;


    while (*sentence != '\n') {
        if (*sentence == ' ') {
            w_count++;
        } else {
            l_count++;
        }
        sentence++;
    }

    return (double) l_count / w_count;
}
