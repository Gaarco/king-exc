#include "line.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_LEN 60

struct node {
    char *word;
    struct node *next;
};

struct node *line = NULL;
int line_len = 0;
int num_words = 0;

void clear_line(void) {
    struct node *tmp;

    while (line) {
        tmp = line;
        line = line->next;
        free(tmp);
    }

    line_len = 0;
    num_words = 0;
}

void add_word(const char *word) {
    struct node *new_node = malloc(sizeof(struct node));
    if (new_node == NULL) {
        printf("error: malloc failed.\n");
        exit(EXIT_FAILURE);
    }

    new_node->word = malloc(strlen(word) + 1);
    if (new_node->word == NULL) {
        printf("error: malloc failed.\n");
        exit(EXIT_FAILURE);
    }

    strcpy(new_node->word, word);
    new_node->next = NULL;

    struct node **pp = &line;
    while (*pp != NULL) {
        pp = &(*pp)->next;
    }
    *pp = new_node;

    if (num_words > 0) {
        line_len++;
    }
    line_len += strlen(word);
    num_words++;
}

int space_remaining(void) { return MAX_LINE_LEN - line_len; }

void write_line(void) {
    int extra_spaces, spaces_to_insert;
    struct node *curr_node = line;

    extra_spaces = MAX_LINE_LEN - line_len;

    while (curr_node != NULL) {
        printf("%s", curr_node->word);
        if (num_words > 1) {
            spaces_to_insert = extra_spaces / (num_words - 1);
            for (int i = 1; i <= spaces_to_insert + 1; i++) {
                putchar(' ');
            }
            extra_spaces -= spaces_to_insert;
        }
        num_words--;
        curr_node = curr_node->next;
    }
    putchar('\n');
}

void flush_line(void) {
    if (line_len > 0) {
        puts(line->word);
    }
}
