#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_WORD_LEN 20
#define LEN(x) \
    ((int) sizeof(x) / sizeof(*x))

int read_line(char str[], int n);
int compare_words(const void *a, const void *b);

int main(void) {
    char **words = malloc(1 * sizeof(char *));
    if (words == NULL) {
        printf("error: malloc failed.\n");
        exit(EXIT_FAILURE);
    }

    int num_words = 0;

    for (int i = 0, count = 1; ; i++) {
        if (count == num_words) {
            words = realloc(words, (++count) * sizeof(char *));
            if (words == NULL) {
                printf("error: malloc failed.\n");
                exit(EXIT_FAILURE);
            }
        }

        words[i] = malloc(MAX_WORD_LEN + 1);
        if (words[i] == NULL) {
            printf("error: malloc failed.\n");
            exit(EXIT_FAILURE);
        }

        printf("Enter word: ");
        int len = read_line(words[i], MAX_WORD_LEN + 1);

        if (len == 0) {
            break;
        }

        num_words++;
    }

    qsort(words, num_words, sizeof(char *), compare_words);

    printf("In sorted order: ");
    for (int i = 0; i < num_words; i++) {
        printf("%s ", words[i]);
    }
    printf("\n");

    free(words);

    return 0;
}

int read_line(char str[], int n) {
    int ch, i = 0;

    while ((ch = getchar()) != '\n') {
        if (i < n) {
            str[i++] = ch;
        }
    }
    str[i] = '\0';
    return i;
}

int compare_words(const void *a, const void *b) {
    return strcmp(*(char **)a, *(char **)b);
}
