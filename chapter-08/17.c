#include <stdio.h>

int main(void) {
    int size, c, r, count = 2;

    printf("This program creates a magic square of a specified size.\n");
    printf("The size must be an odd number between 1 and 99.\n");
    printf("Enter the size of magic square: ");
    scanf("%d", &size);

    int square[size][size];
    for (c = 0; c < size; c++) {
        for (r = 0; r < size; r++) {
            square[c][r] = 0;
        }
    }

    r = 0;
    c = size / 2;
    square[r][c] = 1;
    while (count <= size * size) {
        int old_r = r;
        int old_c = c;

        r = (r - 1 + size) % size;
        c = (c + 1 + size) % size;

        if (square[r][c] != 0) {
            r = (old_r + 1 + size) % size;
            c = old_c;
        }

        square[r][c] = count++;
    }

    for (c = 0; c < size; c++) {
        for (r = 0; r < size; r++) {
            printf("%2d ", square[c][r]);
        }
        printf("\n");
    }

    return 0;
}
