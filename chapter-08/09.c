#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 10
#define DIR 4

int main(void) {
    char letter = 'A', maze[SIZE][SIZE] = { 0 };
    int next_move = 0, blocked_dir[DIR] = { 0 }, x = 0, y = 0;

    srand(time(NULL));

    maze[y][x] = letter;

    while (letter < 'Z') {
        char old_letter = letter;

        int no_dir_avail = 1;
        for (int i = 0; i < DIR; i++) {
            if (blocked_dir[i] == 0) {
                no_dir_avail = 0;
                break;
            }
        }

        if (no_dir_avail) {
            break;
        }

        next_move = rand() % 4;

        switch (next_move) {
            case 0:
                if (y - 1 >= 0 && maze[y - 1][x] == 0) {
                    maze[--y][x] = ++letter;
                }
                break;
            case 1:
                if (x + 1 < SIZE && maze[y][x + 1] == 0) {
                    maze[y][++x] = ++letter;
                }
                break;
            case 2:
                if (y + 1 < SIZE && maze[y + 1][x] == 0) {
                    maze[++y][x] = ++letter;
                }
                break;
            case 3:
                if (x - 1 >= 0 && maze[y][x - 1] == 0) {
                    maze[y][--x] = ++letter;
                }
                break;
            default:
                break;
        }

        if (old_letter != letter) {
            for (int i = 0; i < DIR; i++) {
                blocked_dir[i] = 0;
            }
        } else {
            blocked_dir[next_move] = 1;
        }
    }

    for (y = 0; y < SIZE; y++) {
        for (x = 0; x < SIZE; x++) {
            if (maze[y][x] == 0) {
                maze[y][x] = '.';
            }
            printf("%c ", maze[y][x]);
        }
        printf("\n");
    }

    return 0;
}
