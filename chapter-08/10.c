#include <stdio.h>

#define FLIGHTS 8

int main(void) {
    int hours, minutes, minutes_from_midnight;
    int dep[FLIGHTS] = { 8 * 60, 9 * 60 + 43, 11 * 60 + 19, 12 * 60 + 47, (2 + 12) * 60,
        (3 + 12) * 60 + 45, (7 + 12) * 60, (9 + 12) * 60 + 45 };
    int arr[FLIGHTS] = { 10 * 60 + 16, 11 * 60 + 52, (1 + 12) * 60 + 31, (3 + 12) * 60,
        (4 + 12) * 60 + 8, (5 + 12) * 60 + 55, (9 + 12) * 60 + 20, (11 + 12) * 60 + 58 };

    printf("Enter a 24-hour time: ");
    scanf("%2d:%2d", &hours, &minutes);

    minutes_from_midnight = hours * 60 + minutes;

    for (int i = 0; i < FLIGHTS; i++) {
        if (minutes_from_midnight < dep[i] || minutes_from_midnight >= dep[FLIGHTS - 1]) {
            int user_dep_h = (dep[i] / 60) > 12 ? (dep[i] / 60) - 12 : (dep[i] / 60);
            int user_arr_h = (arr[i] / 60) > 12 ? (arr[i] / 60) - 12 : (arr[i] / 60);
            int user_dep_m = dep[i] % 60;
            int user_arr_m = arr[i] % 60;
            printf("Closest departure time is %d:%.2d %c.m., arriving at %d:%.2d "
                   "%c.m.\n",
                user_dep_h, user_dep_m, (dep[i] / 60) >= 12 ? 'p' : 'a', user_arr_h, user_arr_m,
                (arr[i] / 60) >= 12 ? 'p' : 'a');
            break;
        }
    }

    return 0;
}
