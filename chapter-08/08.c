#include <stdio.h>

#define STUDENT_COUNT 5
#define QUIZ_COUNT 5

int main(void) {
    int student, quiz, qmax, qmin, grades[STUDENT_COUNT][QUIZ_COUNT] = { 0 },
                                   stotal[STUDENT_COUNT] = { 0 }, qtotal[QUIZ_COUNT] = { 0 };

    for (student = 0; student < STUDENT_COUNT; student++) {
        printf("Enter grades for student %d: ", student + 1);
        for (quiz = 0; quiz < QUIZ_COUNT; quiz++) {
            scanf("%d", &grades[student][quiz]);
            stotal[student] += grades[student][quiz];
            qtotal[quiz] += grades[student][quiz];
        }
    }

    printf("\nStudent\tTotal\tAverage\n");
    for (student = 0; student < STUDENT_COUNT; student++) {
        printf(
            "%d\t%d\t%.2f\n", student + 1, stotal[student], (float) stotal[student] / QUIZ_COUNT);
    }

    printf("\nQuiz\tAverage\tMax\tMin\n");
    for (quiz = 0; quiz < QUIZ_COUNT; quiz++) {
        qmax = 0;
        qmin = 999;
        for (student = 0; student < STUDENT_COUNT; student++) {
            if (qmax < grades[student][quiz]) {
                qmax = grades[student][quiz];
            }
            if (qmin > grades[student][quiz]) {
                qmin = grades[student][quiz];
            }
        }
        printf("%d\t%.2f\t%d\t%d\n", quiz + 1, (float) qtotal[quiz] / STUDENT_COUNT, qmax, qmin);
    }

    return 0;
}
