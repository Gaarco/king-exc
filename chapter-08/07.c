#include <stdio.h>

#define SIZE 5

int main(void) {
    int r, c, e, rsum[SIZE] = { 0 }, csum[SIZE] = { 0 };

    for (r = 0; r < SIZE; r++) {
        printf("Enter row %d: ", r + 1);
        for (c = 0; c < SIZE; c++) {
            scanf("%d", &e);
            rsum[r] += e;
            csum[c] += e;
        }
    }

    printf("\nRow totals: ");
    for (r = 0; r < SIZE; r++) {
        printf("%d ", rsum[r]);
    }
    printf("\nColumn totals: ");
    for (c = 0; c < SIZE; c++) {
        printf("%d ", csum[c]);
    }
    printf("\n");

    return 0;
}
