#include <stdio.h>

#define LEN 15

int main(void) {
    char input_num[LEN] = { 0 };

    printf("Enter phone number: ");

    int i = 0;
    while ((input_num[i++] = getchar()) != '\n') { }

    for (int i = 0; i < LEN; i++) {
        switch (input_num[i]) {
            case 'A':
            case 'B':
            case 'C':
                putchar('2');
                break;
            case 'D':
            case 'E':
            case 'F':
                putchar('3');
                break;
            case 'G':
            case 'H':
            case 'I':
                putchar('4');
                break;
            case 'J':
            case 'K':
            case 'L':
                putchar('5');
                break;
            case 'M':
            case 'N':
            case 'O':
                putchar('6');
                break;
            case 'P':
            case 'R':
            case 'S':
                putchar('7');
                break;
            case 'T':
            case 'U':
            case 'V':
                putchar('8');
                break;
            case 'W':
            case 'X':
            case 'Y':
                putchar('9');
                break;
            default:
                putchar(input_num[i]);
        }
    }
    printf("\n");

    return 0;
}
