#include <stdio.h>

#define LEN 20

int main(void) {
    char n, c, sname[LEN] = { 0 };

    printf("Enter a first and last name: ");

    scanf(" %c", &n);
    while ((c = getchar()) != ' ') { }
    while ((c = getchar()) == ' ') { }

    for (int i = 0; c != '\n' && c != ' '; i++) {
        sname[i] = c;
        c = getchar();
    }

    for (int i = 0; i < LEN; i++) {
        printf("%c", sname[i]);
    }
    printf(", %c.\n", n);

    return 0;
}
