#include <stdio.h>

#define LEN 80

int main(void) {
    char c, mess[LEN] = { 0 };
    int shift;

    printf("Enter message to be encrypted: ");
    for (int i = 0; (c = getchar()) != '\n'; i++) {
        mess[i] = c;
    }

    printf("Enter shift amount (1-25): ");
    scanf("%d", &shift);

    printf("Encrypted message: ");
    for (int i = 0; i < LEN; i++) {
        if (mess[i] >= 'A' && mess[i] <= 'Z') {
            putchar(((mess[i] - 'A') + shift) % 26 + 'A');
        } else if (mess[i] >= 'a' && mess[i] <= 'z') {
            putchar(((mess[i] - 'a') + shift) % 26 + 'a');
        } else {
            putchar(mess[i]);
        }
    }
    putchar('\n');

    return 0;
}
