#include <ctype.h>
#include <stdio.h>

int main(void) {
    int alphabet[26] = { 0 };
    char c;

    printf("Enter first word: ");
    while ((c = getchar()) != '\n') {
        if (isalpha(c)) {
            alphabet[toupper(c) - 'A']++;
        }
    }

    printf("Enter second word: ");
    while ((c = getchar()) != '\n') {
        if (isalpha(c)) {
            alphabet[toupper(c) - 'A']--;
        }
    }

    int anagrams = 1;
    for (int i = 0; i < 26; i++) {
        if (alphabet[i] != 0) {
            anagrams = 0;
            break;
        }
    }

    if (anagrams) {
        printf("The words are anagrams.\n");
    } else {
        printf("The words are not anagrams.\n");
    }

    return 0;
}
