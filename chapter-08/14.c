#include <stdio.h>

#define LEN 50

int main(void) {
    char term, sentence[LEN] = { 0 };

    printf("Enter a sentence: ");

    for (int i = 0; i < LEN; i++) {
        char c = getchar();
        if (c == '.' || c == '?' || c == '!') {
            term = c;
            break;
        }
        sentence[i] = c;
    }

    int start_word = LEN - 1;
    for (int i = LEN - 1; i >= 0; i--) {
        if (i == 0 || sentence[i - 1] == ' ') {
            for (int j = i; j < start_word; j++) {
                putchar(sentence[j]);
            }
            putchar(i == 0 ? term : ' ');
            start_word = i - 1;
        }
    }
    putchar('\n');

    return 0;
}
