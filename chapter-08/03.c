#include <stdio.h>

#define LEN 10

int main(void) {
    int digit;
    long n;

    printf("Enter a number: ");
    scanf("%ld", &n);

    while (n > 0) {
        bool digit_seen[LEN] = { false };
        while (n > 0) {
            digit = n % 10;
            if (digit_seen[digit]) {
                break;
            }
            digit_seen[digit] = true;
            n /= 10;
        }

        if (n > 0) {
            printf("Repeated digit\n");
        } else {
            printf("No repeated digit\n");
        }

        printf("Enter a number: ");
        scanf("%ld", &n);
    }

    return 0;
}
