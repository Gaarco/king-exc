#include <ctype.h>
#include <stdio.h>

#define SIZE 30

int main(void) {
    char c, message[SIZE] = { 0 };
    int i = 0;

    printf("Enter a message: ");
    while ((c = getchar()) != '\n' && i < SIZE) {
        message[i++] = c;
    }

    printf("In B1FF-speak: ");
    for (i = 0; i < SIZE; i++) {
        switch (c = toupper(message[i])) {
            case 'A':
                putchar('4');
                break;
            case 'B':
                putchar('8');
                break;
            case 'E':
                putchar('3');
                break;
            case 'I':
                putchar('1');
                break;
            case 'O':
                putchar('0');
                break;
            default:
                putchar(c);
                break;
        }
    }
    printf("!!!!!!!!!!\n");

    return 0;
}
