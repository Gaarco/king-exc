#include "stack.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

int main(void) {
    char c, op1, op2;

    while (true) {
        printf("Enter an RPN expression: ");

        while ((c = getchar()) != '\n') {
            if (isdigit(c)) {
                push(c - '0');
            } else {
                switch (c) {
                    case '+':
                        op1 = pop();
                        op2 = pop();
                        push(op2 + op1);
                        break;
                    case '-':
                        op1 = pop();
                        op2 = pop();
                        push(op2 - op1);
                        break;
                    case '*':
                        op1 = pop();
                        op2 = pop();
                        push(op2 * op1);
                        break;
                    case '/':
                        op1 = pop();
                        op2 = pop();
                        push(op2 / op1);
                        break;
                    case '=':
                        printf("Value of expression: %d\n", pop());
                        make_empty();
                        break;
                    case ' ':
                        break;
                    default:
                        exit(EXIT_SUCCESS);
                        break;
                }
            }
        }
    }

    return 0;
}
