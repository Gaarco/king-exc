#include <stdio.h>

void reduce(int numerator, int denominator, int *reduced_numerator, int *reduced_denominator);

int main(void) {
    int num, den, red_num, red_den;

    printf("Enter a fraction: ");
    scanf("%d/%d", &num, &den);

    reduce(num, den, &red_num, &red_den);

    printf("In lowest terms: %d/%d\n", red_num, red_den);

    return 0;
}

void reduce(int numerator, int denominator, int *reduced_numerator, int *reduced_denominator) {
    int max, min;

    if (numerator > denominator) {
        max = numerator;
        min = denominator;
    } else {
        max = denominator;
        min = numerator;
    }

    while (min != 0) {
        int r = max % min;
        max = min;
        min = r;
    }

    *reduced_numerator = numerator / max;
    *reduced_denominator = denominator / max;
}
