#include <stdio.h>

#define NUM_FLIGHTS 8

int flights[NUM_FLIGHTS][2] = { { 480, 616 }, { 583, 712 }, { 679, 811 }, { 767, 900 },
    { 840, 968 }, { 945, 1075 }, { 1140, 1280 }, { 1305, 1438 } };

void find_closest_flight(int desired_time, int *departure_time, int *arrival_time);

int main(void) {
    int hours, minutes, minutes_from_midnight, dep, arr;

    printf("Enter a 24-hour time: ");
    scanf("%2d:%2d", &hours, &minutes);

    minutes_from_midnight = hours * 60 + minutes;

    find_closest_flight(minutes_from_midnight, &dep, &arr);

    int dep_h = dep / 60 > 12 ? dep / 60 - 12 : dep / 60;
    int arr_h = arr / 60 > 12 ? arr / 60 - 12 : arr / 60;
    printf("Closest departure time is %d:%.2d a.m., arriving at %d:%.2d a.m.\n", dep_h, dep % 60,
        arr_h, arr % 60);

    return 0;
}

void find_closest_flight(int desired_time, int *departure_time, int *arrival_time) {
    for (int i = 0; i < NUM_FLIGHTS - 1; i++) {
        if (desired_time < (flights[i][0] + flights[i + 1][0]) / 2) {
            *departure_time = flights[i][0];
            *arrival_time = flights[i][1];
            return;
        }
    }

    *departure_time = flights[NUM_FLIGHTS - 1][0];
    *arrival_time = flights[NUM_FLIGHTS - 1][1];
}
