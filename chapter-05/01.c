#include <stdio.h>

int main(void) {
    int num, dig;

    printf("Enter a number: ");
    scanf("%d", &num);

    if (num <= 9) {
        dig = 1;
    } else if (num <= 99) {
        dig = 2;
    } else if (num <= 999) {
        dig = 3;
    } else {
        dig = 4;
    }

    printf("The number %d has %d digits\n", num, dig);

    return 0;
}
