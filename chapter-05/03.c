#include <stdio.h>

// This project's text was not clear enough, I'm not sure this is correct

int main(void) {
    int act;
    float commission, commission2, price, value;

    printf("Enter number of actions: ");
    scanf("%d", &act);

    printf("Enter price per action: ");
    scanf("%f", &price);

    value = price * act;
    if (value < 2500.0f) {
        commission = 30.0f + 0.017f * value;
    } else if (value < 6250.0f) {
        commission = 56.0f + 0.0066f * value;
    } else if (value < 20000.0f) {
        commission = 76.0f + 0.0034f * value;
    } else if (value < 50000.0f) {
        commission = 100.0f + 0.0022f * value;
    } else if (value < 500000.0f) {
        commission = 155.0f + 0.0011f * value;
    } else {
        commission = 255.0f + 0.0009f * value;
    }

    if (commission < 39.0f) {
        commission = 39.0f;
    }

    if (act < 2000) {
        commission2 = 33.3f * act;
    } else {
        commission2 = 33.2f * act;
    }

    printf("Commission: $%.2f\n", commission);
    printf("Commission of rival broker: $%.2f\n", commission2);

    return 0;
}
