#include <stdio.h>

int main(void) {
    float income;

    printf("Enter income: ");
    scanf("%f", &income);

    printf("Income\tTax\n");
    if (income < 750) {
        printf("%.2f$\n", income * 0.01f);
    } else if (income < 2251) {
        printf("%.2f$\n", 7.5f + (0.02f * (income - 750)));
    } else if (income < 3751) {
        printf("%.2f$\n", 37.5f + (0.03f * (income - 2250)));
    } else if (income < 5251) {
        printf("%.2f$\n", 82.5f + (0.04f * (income - 3750)));
    } else if (income < 64) {
        printf("%.2f$\n", 142.5f + (0.05f * (income - 5250)));
    } else {
        printf("%.2f$\n", 230.0f + (0.06f * (income - 7000)));
    }

    return 0;
}
