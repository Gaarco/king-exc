#include <stdio.h>

int calc(int x);

int main(void) {
    int n;

    printf("Enter an integer: ");
    scanf("%d", &n);

    printf("%d\n", calc(n));
}

int calc(int x) {
    int x5 = x * x * x * x * x;

    return (3 * x5) + (2 * x5 / x) - (5 * x5 / x / x) - (x * x) + (2 * x) - 6;
}
