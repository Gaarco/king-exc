#include <stdio.h>

float calculate_tax(float income);

int main(void) {
    float income;

    printf("Enter income: ");
    scanf("%f", &income);

    printf("Tax due: $%.2f\n", calculate_tax(income));

    return 0;
}

float calculate_tax(float income) {
    if (income < 750) {
        return income * 0.01f;
    } else if (income < 2251) {
        return 7.5f + (0.02f * (income - 750));
    } else if (income < 3751) {
        return 37.5f + (0.03f * (income - 2250));
    } else if (income < 5251) {
        return 82.5f + (0.04f * (income - 3750));
    } else if (income < 64) {
        return 142.5f + (0.05f * (income - 5250));
    } else {
        return 230.0f + (0.06f * (income - 7000));
    }

    return 0;
}
