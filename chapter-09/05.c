#include <stdio.h>

void create_magic_square(int n, char magic_square[n][n]);

void print_magic_square(int n, char magic_square[n][n]);

int main(void) {
    int size;

    printf("This program creates a magic square of a specified size.\n");
    printf("The size must be an odd number between 1 and 99.\n");
    printf("Enter the size of magic square: ");
    scanf("%d", &size);

    char square[size][size];

    create_magic_square(size, square);
    print_magic_square(size, square);

    return 0;
}

void create_magic_square(int n, char magic_square[n][n]) {
    for (int c = 0; c < n; c++) {
        for (int r = 0; r < n; r++) {
            magic_square[c][r] = 0;
        }
    }

    int r = 0;
    int c = n / 2;
    int count = 2;
    magic_square[r][c] = 1;
    while (count <= n * n) {
        int old_r = r;
        int old_c = c;

        r = (r - 1 + n) % n;
        c = (c + 1 + n) % n;

        if (magic_square[r][c] != 0) {
            r = (old_r + 1 + n) % n;
            c = old_c;
        }

        magic_square[r][c] = count++;
    }
}

void print_magic_square(int n, char magic_square[n][n]) {
    for (int c = 0; c < n; c++) {
        for (int r = 0; r < n; r++) {
            printf("%2d ", magic_square[c][r]);
        }
        printf("\n");
    }
}
