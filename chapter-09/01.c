#include <stdio.h>

#define LEN 10

void selection_sort(int n, int v[n]);

int main(void) {
    int numbers[LEN] = { 0 };

    printf("Enter %d integers: ", LEN);
    for (int i = 0; i < LEN; i++) {
        scanf("%d", &numbers[i]);
    }

    selection_sort(LEN, numbers);

    printf("Sorted integers: ");
    for (int i = 0; i < LEN; i++) {
        printf("%d ", numbers[i]);
    }
    printf("\n");

    return 0;
}

void selection_sort(int n, int v[n]) {
    if (n == 0) {
        return;
    }

    int largest = 0;
    for (int i = 1; i < n; i++) {
        if (v[i] > v[largest]) {
            largest = i;
        }
    }

    int tmp = v[n - 1];
    v[n - 1] = v[largest];
    v[largest] = tmp;

    selection_sort(n - 1, v);
}
