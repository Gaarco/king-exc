#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int roll_dice(void);

bool play_game(void);

int main(void) {
    int wins = 0, games = 0;
    char selection;

    srand(time(NULL));

    do {
        games++;

        if (play_game()) {
            wins++;
        }

        printf("\nPlay again? ");
        scanf(" %c", &selection);
        printf("\n");
    } while (toupper(selection) == 'Y');

    printf("Wins: %d Losses: %d\n", wins, games - wins);

    return 0;
}

int roll_dice(void) {
    int d1 = rand() % 6 + 1;
    int d2 = rand() % 6 + 1;

    return d1 + d2;
}

bool play_game(void) {
    int point;

    int d = roll_dice();

    printf("You rolled %d\n", d);

    if (d == 7 || d == 11) {
        printf("You win!\n");
        return true;
    }

    if (d == 2 || d == 3 || d == 12) {
        printf("You lose!\n");
        return false;
    }

    printf("Your point is %d\n", point = d);

    while (true) {
        d = roll_dice();

        printf("You rolled %d\n", d);

        if (d == point) {
            printf("You win!\n");
            return true;
        }

        if (d == 7) {
            printf("You lose!\n");
            return false;
        }
    }
}
