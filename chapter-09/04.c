#include <ctype.h>
#include <stdio.h>

#define LEN 26

void read_word(int counts[LEN]);

bool equal_array(int counts1[LEN], int counts2[LEN]);

int main(void) {
    int counts1[LEN] = { 0 }, counts2[LEN] = { 0 };

    printf("Enter first word: ");
    read_word(counts1);

    printf("Enter second word: ");
    read_word(counts2);

    if (equal_array(counts1, counts2)) {
        printf("The words are anagrams.\n");
    } else {
        printf("The words are not anagrams.\n");
    }

    return 0;
}

void read_word(int counts[LEN]) {
    char c;
    while ((c = getchar()) != '\n') {
        if (isalpha(c)) {
            counts[toupper(c) - 'A']++;
        }
    }
}

bool equal_array(int counts1[LEN], int counts2[LEN]) {
    for (int i = 0; i < LEN; i++) {
        if (counts1[i] != counts2[i]) {
            return false;
        }
    }
    return true;
}
