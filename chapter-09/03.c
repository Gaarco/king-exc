#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 10
#define DIR 4

void generate_random_walk(char walk[10][10]);

void print_array(char walk[10][10]);

int main(void) {
    char maze[SIZE][SIZE];

    generate_random_walk(maze);
    print_array(maze);

    return 0;
}

void generate_random_walk(char walk[10][10]) {
    char letter;
    int row, col, next_move = 0, blocked_dir[DIR] = { 0 };

    for (row = 0; row < SIZE; row++) {
        for (col = 0; col < SIZE; col++) {
            walk[row][col] = '.';
        }
    }

    srand(time(NULL));

    col = 0;
    row = 0;
    letter = 'A';
    walk[row][col] = letter;

    while (letter < 'Z') {
        char old_letter = letter;

        int no_dir_avail = 1;
        for (int i = 0; i < DIR; i++) {
            if (blocked_dir[i] == 0) {
                no_dir_avail = 0;
                break;
            }
        }

        if (no_dir_avail) {
            break;
        }

        next_move = rand() % 4;

        switch (next_move) {
            case 0:
                if (row - 1 >= 0 && walk[row - 1][col] == '.') {
                    walk[--row][col] = ++letter;
                }
                break;
            case 1:
                if (col + 1 < SIZE && walk[row][col + 1] == '.') {
                    walk[row][++col] = ++letter;
                }
                break;
            case 2:
                if (row + 1 < SIZE && walk[row + 1][col] == '.') {
                    walk[++row][col] = ++letter;
                }
                break;
            case 3:
                if (col - 1 >= 0 && walk[row][col - 1] == '.') {
                    walk[row][--col] = ++letter;
                }
                break;
            default:
                break;
        }

        if (old_letter != letter) {
            for (int i = 0; i < DIR; i++) {
                blocked_dir[i] = 0;
            }
        } else {
            blocked_dir[next_move] = 1;
        }
    }
}

void print_array(char walk[10][10]) {
    for (int row = 0; row < SIZE; row++) {
        for (int col = 0; col < SIZE; col++) {
            printf("%c ", walk[row][col]);
        }
        printf("\n");
    }
}
