#include <stdio.h>

int main(void) {
    int first, second, third;

    printf("Enter phone number [(xxx) xxx-xxxx]: ");
    scanf("(%3d) %3d-%4d", &first, &second, &third);

    printf("You entered %d.%d.%d\n", first, second, third);

    return 0;
}
