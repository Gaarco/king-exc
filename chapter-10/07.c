#include <ctype.h>
#include <stdio.h>

#define MAX_DIGITS 10
#define DIGIT_SIZE 4

int segment_array[MAX_DIGITS][7] = { { 1, 1, 1, 1, 1, 1, 0 }, { 0, 1, 1, 0, 0, 0, 0 },
    { 1, 1, 0, 1, 1, 0, 1 }, { 1, 1, 1, 1, 0, 0, 1 }, { 0, 1, 1, 0, 0, 1, 1 },
    { 1, 0, 1, 1, 0, 1, 0 }, { 1, 0, 1, 1, 1, 1, 1 }, { 1, 1, 1, 0, 0, 0, 0 },
    { 1, 1, 1, 1, 1, 1, 1 }, { 1, 1, 1, 1, 0, 1, 1 } };
char segment_pos[7][2] = { { 0, 1 }, { 1, 2 }, { 2, 2 }, { 2, 1 }, { 2, 0 }, { 1, 0 }, { 1, 1 } };
char digits[DIGIT_SIZE][MAX_DIGITS * DIGIT_SIZE];

void clear_digits_array(void);
void process_digit(int digit, int position);
void print_digits_array(void);

int main(void) {
    char c;

    clear_digits_array();

    printf("Enter a number: \n");

    int count = 0;
    while ((c = getchar()) != '\n' && count < MAX_DIGITS) {
        if (isdigit(c)) {
            process_digit(c - '0', count * DIGIT_SIZE);
            count++;
        }
    }

    print_digits_array();

    return 0;
}

void clear_digits_array(void) {
    for (int i = 0; i < DIGIT_SIZE; i++) {
        for (int j = 0; j < MAX_DIGITS * DIGIT_SIZE; j++) {
            digits[i][j] = ' ';
        }
    }
}

void process_digit(int digit, int position) {
    for (int i = 0; i < 7; i++) {
        if (segment_array[digit][i]) {
            int row = segment_pos[i][0];
            int col = segment_pos[i][1];

            digits[row][position + col] = col == 1 ? '_' : '|';
        }
    }
}

void print_digits_array(void) {
    for (int i = 0; i < DIGIT_SIZE; i++) {
        for (int j = 0; j < MAX_DIGITS * DIGIT_SIZE; j++) {
            printf("%c", digits[i][j]);
        }
        printf("\n");
    }
}
