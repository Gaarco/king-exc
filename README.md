# King ExC

Solutions to projects from _Kim N. King's_ textbook:
[C Programming: A Modern Approach, 2nd Edition](http://knking.com/books/c2/index.html).

Each project uses only concepts explained in the current chapter or in the previous chapters.
