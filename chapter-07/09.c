#include <ctype.h>
#include <stdio.h>

int main(void) {
    int hours, minutes;
    char ap;

    printf("Enter a 12-hour time: ");
    scanf("%2d:%2d %c", &hours, &minutes, &ap);

    hours = (toupper(ap) == 'P' ? 12 : 0) + hours;

    printf("Equivalent 24-hour time: %.2d:%.2d\n", hours, minutes);

    return 0;
}
