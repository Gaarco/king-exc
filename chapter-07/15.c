#include <stdio.h>

int main(void) {
    int x, fac = 1;

    printf("Enter a positive integer: ");
    scanf("%d", &x);

    for (int i = 2; i <= x; i++) {
        fac *= i;
    }

    printf("Factorial of %d: %d\n", x, fac);

    return 0;
}
