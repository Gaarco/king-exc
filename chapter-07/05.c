#include <ctype.h>
#include <stdio.h>

int main(void) {
    char ch;
    int val = 0;

    printf("Enter a word: ");

    while ((ch = getchar()) != '\n') {
        switch (toupper(ch)) {
            case 'D':
            case 'G':
                val += 2;
                break;
            case 'B':
            case 'C':
            case 'M':
            case 'P':
                val += 3;
                break;
            case 'F':
            case 'H':
            case 'V':
            case 'W':
            case 'Y':
                val += 4;
                break;
            case 'K':
                val += 5;
                break;
            case 'J':
            case 'X':
                val += 8;
                break;
            case 'Q':
            case 'Z':
                val += 10;
                break;
            default:
                val += 1;
        }
    }

    printf("Scrabble value: %d\n", val);

    return 0;
}
