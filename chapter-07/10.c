#include <ctype.h>
#include <stdio.h>

int main(void) {
    int vc = 0;
    char c;

    printf("Enter a sentence: ");
    while ((c = toupper(getchar())) != '\n') {
        if (c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U') {
            vc++;
        }
    }

    printf("Your sentence contains %d vowels\n", vc);

    return 0;
}
