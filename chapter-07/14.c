#include <math.h>
#include <stdio.h>

#define EPSILON 0.00001f

int main(void) {
    float x, y = 1.0f;

    printf("Enter a positive number: ");
    scanf("%f", &x);

    while (fabs(y - (y + x / y) / 2) > EPSILON * y) {
        y = (y + x / y) / 2;
    }

    printf("Square root: %f\n", y);

    return 0;
}
