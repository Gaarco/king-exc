#include <stdio.h>

int main(void) {
    char n, c;

    printf("Enter a first and last name: ");

    scanf(" %c", &n);
    while ((c = getchar()) != ' ') { }
    while ((c = getchar()) == ' ') { }

    do {
        putchar(c);
    } while ((c = getchar()) != '\n' && c != ' ');

    printf(", %c.\n", n);

    return 0;
}
