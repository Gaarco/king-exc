#include <stdio.h>

int main(void) {
    float avg_len;
    int w_count = 1, l_count = 0;
    char c;

    printf("Enter a sentence: ");
    while ((c = getchar()) != '\n') {
        if (c == ' ') {
            w_count++;
        } else {
            l_count++;
        }
    }

    avg_len = (float) l_count / w_count;
    printf("Average word length: %.1f\n", avg_len);

    return 0;
}
